import ContactForm from '../../components/ContactForm/ContactForm';
import classes from './Contact.module.css';

const Contact = () => {
  return (
    <main className={classes.contact}>
      <ContactForm />
    </main>
  );
};

export default Contact;
