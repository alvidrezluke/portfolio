import classes from './Portfolio.module.css';
import Collection from '../../components/Collection/Collection';

const Portfolio = () => {
  return (
    <main className={classes.portfolio}>
      <Collection />
    </main>
  );
};

export default Portfolio;
