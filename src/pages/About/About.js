import classes from './About.module.css';

const About = () => {
  return (
    <main className={classes.about}>
      <h2>Hi! I'm Luke Alvidrez</h2>
      <p>
        I'm a web developer who specializes in React applications. I am
        confident in React and NextJS. I am based in the South-East United
        States. I am always looking to improve my skills.
      </p>
      <p>
        I enjoy playing video games or 3D modeling and printing when I'm not
        developing websites.
      </p>
      <p>
        If you would like to reach out to me you can contact me using the
        contact form, sending an email to alvidrezluke@gmail.com, or by phone at
        (256)600-0730.
      </p>
    </main>
  );
};

export default About;
