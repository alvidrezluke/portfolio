import Card from './Card/Card';
import Images from '../../assets/images';
import classes from './Collection.module.css';

const Collection = () => {
  const schoolCounselingCorner = Images['schoolCounselingCorner'];
  return (
    <main className={classes.collection}>
      <Card
        img={schoolCounselingCorner.image}
        title={schoolCounselingCorner.title}
        description={schoolCounselingCorner.description}
        link={schoolCounselingCorner.link}
      />
      <Card
        img={schoolCounselingCorner.image}
        title={schoolCounselingCorner.title}
        description={schoolCounselingCorner.description}
        link={schoolCounselingCorner.link}
      />
      <Card
        img={schoolCounselingCorner.image}
        title={schoolCounselingCorner.title}
        description={schoolCounselingCorner.description}
        link={schoolCounselingCorner.link}
      />
    </main>
  );
};

export default Collection;
