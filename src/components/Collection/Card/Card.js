import { useState } from 'react';

import classes from './Card.module.css';

const Card = (props) => {
  const [hovered, setHovered] = useState(false);

  const imageClasses = `${classes['card__image']} ${
    hovered ? classes['card__image--hover'] : ''
  }`;
  const descriptionClasses = `${classes['card__description']} ${
    hovered ? classes['card__description--hover'] : ''
  }`;

  const handleMouseEnter = () => {
    setHovered(true);
  };
  const handleMouseLeave = () => {
    setHovered(false);
  };
  const handleClick = () => {
    window.location = props.link;
  };

  return (
    <section
      className={classes.card}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}
      onClick={handleClick}
    >
      <img src={props.img} alt={props.title} className={imageClasses} />
      <div className={descriptionClasses}>
        <h4>{props.title}</h4>
        <p>{props.description}</p>
      </div>
    </section>
  );
};

export default Card;
