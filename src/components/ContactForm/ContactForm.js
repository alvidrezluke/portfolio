import { useRef, useState } from 'react';
import emailjs from 'emailjs-com';
import classes from './ContactForm.module.css';

const ContactForm = () => {
  const [sending, setSending] = useState(false);
  const [sent, setSent] = useState(false);
  const [err, setErr] = useState();

  const nameInput = useRef();
  const emailInput = useRef();
  const messageInput = useRef();

  const checkFilled = (val) => {
    if (val.trim().length !== 0) return true;
    else return false;
  };

  const onSubmitHandler = (event) => {
    event.preventDefault();
    setSending(true);
    const nameValue = nameInput.current.value;
    const emailValue = emailInput.current.value;
    const messageValue = messageInput.current.value;
    const isNameValid = checkFilled(nameValue);
    const isEmailValid = checkFilled(emailValue);
    const isMessageValid = checkFilled(messageValue);
    if (!isNameValid) {
      setErr('Name can not be empty!');
      return;
    } else if (!isEmailValid) {
      setErr('Email can not be empty!');
      return;
    } else if (!isMessageValid) {
      setErr('Message can not be empty!');
      return;
    }
    const templateParams = {
      from_name: nameValue,
      email: emailValue,
      message: messageValue,
    };
    emailjs
      .send(
        'service_u3yegpq',
        'template_7d8anpk',
        templateParams,
        'user_MyaO2QsL6Kumq6wp4ThTs'
      )
      .then((res) => {
        setSending(false);
        setSent(true);
      })
      .catch((err) => {
        setErr(err);
      });
  };

  let buttonText = 'Submit';
  if (sending) buttonText = 'Sending...';
  if (sent) buttonText = 'Sent';

  return (
    <form className={classes.form} onSubmit={onSubmitHandler}>
      <label htmlFor='name'>Name:</label>
      <input id='name' ref={nameInput} type='text' />
      <label htmlFor='email'>Email:</label>
      <input id='email' ref={emailInput} type='email' />
      <label htmlFor='message'>Message:</label>
      <textarea id='message' ref={messageInput} type='text' />
      <button type='submit'>{buttonText}</button>
      {err ? <p className={classes.err}>{err}</p> : null}
    </form>
  );
};

export default ContactForm;
