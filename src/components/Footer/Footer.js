import { Link } from 'react-router-dom';
import classes from './Footer.module.css';

const Footer = () => {
  return (
    <footer className={classes.footer}>
      <div className={classes['footer__credits']}>
        <h5 className={classes['footer__heading']}>
          Designed and Developed by Luke Alvidrez. Inspired by{' '}
          <a href='https://www.haydensnow.com/about'>Hayden Snow</a>.
        </h5>
        <small className={classes['footer__copyright']}>
          &copy; All Rights Reserved. <br />
          Code available at{' '}
          <a
            href='https://gitlab.com/alvidrezluke/portfolio'
            className={classes['footer__sourceCode']}
          >
            Gitlab
          </a>
          .
        </small>
      </div>
      <nav className={classes['footer__navigation']}>
        <h5>Navigation</h5>
        <Link to='/'>Portfolio</Link>
        <Link to='/about'>About</Link>
        <Link to='/contact'>Contact</Link>
      </nav>
      <div className={classes['footer__contact']}>
        <h5>Contact</h5>
        <ul>
          <li>
            <a href='tel:+12566000730'>Phone: (256)600-0730</a>
          </li>
          <li>
            <a href='mailto:alvidrezluke@gmail.com'>
              Email: alvidrezluke@gmail.com
            </a>
          </li>
        </ul>
      </div>
    </footer>
  );
};

export default Footer;
