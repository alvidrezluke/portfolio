import { render, screen } from '@testing-library/react';
import Navbar from './Navbar';

test('renders all 3 NavLinks', () => {
  render(<Navbar />);
  const portfolioLink = screen.getByText(/portfolio/i);
  const aboutLink = screen.getByText(/about/i);
  const contactLink = screen.getByText(/contact/i);
  expect(portfolioLink).toBeInTheDocument();
  expect(aboutLink).toBeInTheDocument();
  expect(contactLink).toBeInTheDocument();
});
