import { NavLink } from 'react-router-dom';
import classes from './Navbar.module.css';

const Navbar = () => {
  return (
    <header className={classes.navigation}>
      <h3 className={classes['navigation__heading']}>Luke Alvidrez</h3>
      <nav className={classes['navigation__links']}>
        <NavLink
          exact
          to='/'
          activeClassName={classes['navigation__link--active']}
        >
          Portfolio
        </NavLink>
        <NavLink
          to='/about'
          activeClassName={classes['navigation__link--active']}
        >
          About
        </NavLink>
        <NavLink
          to='/contact'
          activeClassName={classes['navigation__link--active']}
        >
          Contact
        </NavLink>
      </nav>
    </header>
  );
};

export default Navbar;
