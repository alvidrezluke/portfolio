import SchoolCounselingCorner from './images/SchoolCounselingCorner.png';

const Designs = {
  schoolCounselingCorner: {
    image: SchoolCounselingCorner,
    title: 'The School Counseling Corner',
    description:
      'A website in order to provide resources for high school students.',
    link: 'https://theschoolcounselingcorner.com',
  },
};

export default Designs;
